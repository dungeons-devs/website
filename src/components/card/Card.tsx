import { useEffect, useReducer, useState } from "react";
import ExpandableInfo from "@/components/card/ExpandableInfo"

type CharacterStats = {
    strength: number;
    dexterity: number;
    constitution: number;
    intelligence: number;
    wisdom: number;
    charisma: number;
    armorClass: number;
    hitPoints: number;
};

type CharacterInfo = {
    characterName: string;
    campaignNum: number;
    campaignTime: string;
    id: number;
    characterStats: CharacterStats;
}

export default function Card(props: {initOpen: boolean, characterInfo: CharacterInfo})
{
    const initialFieldValues =
    {
        open: props.initOpen,
        start: false,
    }

    const [fieldValues, setFieldValues] = useReducer(
        (state: any, newState: any) => ({ ...state, ...newState }),
        initialFieldValues,
    );

    // the div that holds the expandable info
    let expandDiv;

    // variable that holds the expandable info component
    const expandInfo = <ExpandableInfo characterStats={props.characterInfo.characterStats}></ExpandableInfo>;

    if (fieldValues.start && fieldValues.open) // If expand has been clicked and toggled to open
    {
        expandDiv =
        <div className="open">
            {expandInfo}
        </div>;
    }
    else if (fieldValues.start && !fieldValues.open) // If expand has been clicked and toggled to close
    {
        expandDiv =
        <div className="close">
            {expandInfo}
        </div>;
    }
    else if (!fieldValues.start && !fieldValues.open) // Page loaded and started as closed
    {
        expandDiv = <></>;
    }
    else // Page loaded and started as open
    {
        expandDiv =
        <div className="start">
            {expandInfo}
        </div>;
    }

    return (
        <>
            <div className ="card">
                <div className="cardHeader">
                    <h4>{props.characterInfo.characterName} <span className="floatRight">{props.characterInfo.campaignNum}</span></h4>
                </div>

                {expandDiv}

                <div className="cardFooter">
                    <h4>{props.characterInfo.campaignTime}
                        <span className="floatRight spanExpand" onClick={() => setFieldValues(
                            {
                                open: !fieldValues.open,
                                start: true
                            })}>
                            {fieldValues.open ? "+  " : "-   "}expand
                        </span>
                    </h4>
                </div>
            </div>
        </>
    );
}
