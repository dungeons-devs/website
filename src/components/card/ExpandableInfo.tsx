type CharacterStats = {
    strength: number;
    dexterity: number;
    constitution: number;
    intelligence: number;
    wisdom: number;
    charisma: number;
    armorClass: number;
    hitPoints: number;
};

export default function ExpandableInfo(props: {characterStats: CharacterStats }) {
    return (
        <div className="expandedStats">
            <div className ="statsInfo">
                <p>Strength: {props.characterStats.strength}</p>
                <p>Dexterity: {props.characterStats.strength}</p>
                <p>Constitution: {props.characterStats.constitution}</p>
                <p>Intelligence: {props.characterStats.intelligence}</p>
                <p>Wisdom: {props.characterStats.wisdom}</p>
                <p>Charisma: {props.characterStats.charisma}</p>
            </div>
            <div className="healthInfo">
                <p>Armor Class: {props.characterStats.armorClass}</p>
                <p>Hit Points: {props.characterStats.hitPoints}</p>
            </div>
        </div>
    );
}
