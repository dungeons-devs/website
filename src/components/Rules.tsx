export default function Rules(props: {children: string, br:boolean})
{
    return (
        <li>
            <p>{props.children}</p>
            {props.br && "---\n"}
        </li>
    );
}

Rules.defaultProps = {
    br: true,
    children: ""
};
