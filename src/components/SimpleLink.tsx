import { ReactElement, JSXElementConstructor, ReactFragment, ReactPortal } from "react"

export default function SimpleLink(props: { title: string, link: string })
{
    return (
        <a href={props.link} rel="me" target="_blank">
            {props.title}
        </a>
    );
}
