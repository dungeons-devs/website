import Link from "next/link"

export default function Navigation()
{
    return (
        <div>
            <ul className="navigation">
                <li><Link href="/">Home</Link></li>
                <li><Link href="/emojis">Emojis</Link></li>
                <li><Link href="/rules">Rules & Tips</Link></li>
                <li><Link href="/campform">Submit New Campaigns</Link></li>
                <li><Link href="/characterSheet">Mock Character Sheet</Link></li>
                <li><Link href="#links">Links</Link></li>
            </ul>
        </div>
    );
}
