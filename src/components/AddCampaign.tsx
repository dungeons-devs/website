import { useState } from "react"
import FormSection from "@/components/FormSection"

export default function AddCampaign(props: { onAdd: Function }) {
    const [campaignNum, setNum] = useState(0)
    const [campaignTime, setTime] = useState('')
    const [characterName, setName] = useState('')
    const [strength, setStrength] = useState(10)
    const [dexterity, setDexterity] = useState(10)
    const [constitution, setConstitution] = useState(10)
    const [intelligence, setIntelligence] = useState(10)
    const [wisdom, setWisdom] = useState(10)
    const [charisma, setCharisma] = useState(10)
    const [armorClass, setArmor] = useState(10)
    const [hitPoints, setHitPoints] = useState(10)

    type CampaignNoId = {
        campaignNum: number;
        campaignTime: string;
        characterName: string;
        characterStats: [
            [string, number],
            [string, number],
            [string, number],
            [string, number],
            [string, number],
            [string, number],
            [string, number],
            [string, number]
        ];
    };

    const onSubmit = (e: any) => {
        e.preventDefault()

        if (!campaignTime) {
            alert('Please add a campaign time')
            return
        } else if (!characterName) {
            alert('Please add a character name')
        }

        let newCamp:CampaignNoId = {
            campaignNum: campaignNum,
            campaignTime: campaignTime,
            characterName: characterName,
            characterStats: [
                ["strength", strength],
                ["dexterity", dexterity],
                ["constitution", constitution],
                ["intelligence", intelligence],
                ["wisdom", wisdom],
                ["charisma", charisma],
                ["armorClass", armorClass],
                ["hitPoints", hitPoints]
            ]
        }

        props.onAdd(newCamp)

        // resetting values
        setNum(0)
        setTime('')
        setName('')
        setStrength(10)
        setDexterity(10)
        setConstitution(10)
        setIntelligence(10)
        setWisdom(10)
        setCharisma(10)
        setArmor(10)
        setHitPoints(10)
    }

    return (
        <form className='add-form' onSubmit={onSubmit}>
            <FormSection isNum={true} type='number' placeholder="Add Campaign Number" value={campaignNum} stateFunc={setNum}>Campaign Number</FormSection>
            <FormSection placeholder="Add Day & Time" value={campaignTime} stateFunc={setTime}>Campaign Time</FormSection>
            <FormSection placeholder="Set Character Name" value={characterName} stateFunc={setName}>Character Name</FormSection>
            <FormSection isNum={true} placeholder="Set Character Strength" value={strength} stateFunc={setStrength}>Character Strength</FormSection>
            <FormSection isNum={true} placeholder="Set Character Dexterity" value={dexterity} stateFunc={setDexterity}>Character Dexterity</FormSection>
            <FormSection isNum={true} placeholder="Set Character Constitution" value={constitution} stateFunc={setConstitution}>Character Constitution</FormSection>
            <FormSection isNum={true} placeholder="Set Character Intelligence" value={intelligence} stateFunc={setIntelligence}>Character Intelligence</FormSection>
            <FormSection isNum={true} placeholder="Set Character Wisdom" value={wisdom} stateFunc={setWisdom}>Character Wisdom</FormSection>
            <FormSection isNum={true} placeholder="Set Character Charisma" value={charisma} stateFunc={setCharisma}>Character Charisma</FormSection>
            <FormSection isNum={true} placeholder="Set Character Armor Class" value={armorClass} stateFunc={setArmor}>Character Armor Class</FormSection>
            <FormSection isNum={true} placeholder="Set Character Hit Points" value={hitPoints} stateFunc={setHitPoints}>Character Hit Points</FormSection>

            <input type='submit' value='Save Campaign' className='btn btn-block' />
        </form>
    )
}
