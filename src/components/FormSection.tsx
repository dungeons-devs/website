export default function FormSection(props: {children: string, value: string|number, placeholder: string, type: string, isNum: boolean, stateFunc: Function })
{

    return (
        <div className='form-control'>
                <label>{props.children}</label>
                <input
                    type={props.isNum ? 'number' : 'text'}
                    placeholder={props.placeholder}
                    value={props.value}
                    onChange={props.isNum ?
                        (e) => props.stateFunc(Number(e.target.value))
                        :
                        (e) => props.stateFunc(e.target.value)}
                />
        </div>
    );
}

FormSection.defaultProps = {
    children: "",
    isNum: false,
    type: ""
};
