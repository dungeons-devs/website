import ExpandableInfo from "./card/ExpandableInfo";

type CharacterStats = {
    strength: number;
    dexterity: number;
    constitution: number;
    intelligence: number;
    wisdom: number;
    charisma: number;
    armorClass: number;
    hitPoints: number;
};

type CharacterInfo = {
    characterName: string;
    campaignNum: number;
    campaignTime: string;
    id: number;
    characterStats: CharacterStats;
}

export default function CampInfo(props: { characterInfo: CharacterInfo, deleteCampaign: Function }) {
    return (
        <>
            <div className="card">
                <div className="cardHeader">
                    <h4>{props.characterInfo.characterName} <span className="floatRight">id {props.characterInfo.id}</span></h4>
                </div>
                <div className="start">
                    <ExpandableInfo characterStats={props.characterInfo.characterStats}></ExpandableInfo>
                </div>

                <div className="cardFooter">
                    <h4>{props.characterInfo.campaignTime}
                        <span className="floatRight spanExpand" onClick={() => props.deleteCampaign(props.characterInfo.id)}>
                            Delete
                        </span>
                    </h4>
                </div>
            </div>
        </>
    );
}
