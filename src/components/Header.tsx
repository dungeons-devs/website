export default function Header()
{
    return (
        <div>
            <h1><a href="https://mastodon.social/@dungeons" rel="me" target="_blank">Dungeons</a></h1>
        </div>
    );
}
