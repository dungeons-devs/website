import SimpleLink from "@/components/SimpleLink"

export default function Footer()
{
    return (
        <div className ="section">
            <h2 id="links"><a href="#links">Links</a></h2>
            <ul>
                <li>
                    <SimpleLink title="Dungeons Mastodon Account" link="https://mastodon.social/@dungeons"></SimpleLink>
                </li>
                <li>
                    <SimpleLink title="Author Mastodon Account" link="https://mastodon.social/@astrelion"></SimpleLink>
                </li>
                <li>
                    <SimpleLink title="Source Code" link="https://gitlab.com/ASTRELION/dungeons-bot"></SimpleLink>
                </li>
                <li>
                    <SimpleLink title="D&D 5e SRD" link="https://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf"></SimpleLink>
                </li>
                <li>
                    <SimpleLink title="D&D 5e SRD API" link="https://dnd5eapi.co"></SimpleLink>
                </li>
                <li>
                    <SimpleLink title="Mastodon API" link="https://github.com/halcy/Mastodon.py"></SimpleLink>
                </li>
                <li>
                    <SimpleLink title="License" link="https://gitlab.com/ASTRELION/dungeons-bot/-/raw/main/LICENSE"></SimpleLink>
                </li>
            </ul>
        </div>
    );
}
