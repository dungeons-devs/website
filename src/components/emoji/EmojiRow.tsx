export default function EmojiRow(props: {emoji: string, emojiDesc: string })
{
    return (
        <tr>
            <th>{props.emoji}</th>
            <th>{props.emojiDesc}</th>
        </tr>
    );
}
