export default function EmojiTitle()
{
    return (
        <div className="header">
            <h1><a href="https://mastodon.social/@dungeons" rel="me" target="_blank">Dungeons</a></h1>
        </div>
    );
}
