import EmojiRow from "@/components/emoji/EmojiRow"
import React, { useEffect, useState } from 'react';

type Emoji = {
    emoji: string;
    emojiDesc: string;
};

let updateOutside

export default function EmojiCollapsible(props: { emojis: Array<Emoji>, tableTitle: string}) {
    const [open, setOpen] = useState(true);


    const classNormalized = props.tableTitle.toLowerCase().replaceAll( " ", "");
    return (
        <>
            <thead className="collapsibleHead" onClick={() => setOpen(!open)}>
                <tr>
                    <td className="table-title setTableWidth" colSpan={2}>{open ? "+  " : "-   "}{props.tableTitle} </td>
                    {/*<td><hr /></td>*/}
                </tr>
            </thead>

            {open ?
                <tbody className={classNormalized + " collapsibleBody"}>
                    {props.emojis.map(e => createComponent(e))}
                </tbody>
                :
                <></>
                /*could later be used to animated */
            }
        </>
    );
}

function createComponent(emojis: Emoji)
{
    return  <EmojiRow emoji={emojis.emoji} emojiDesc={emojis.emojiDesc}></EmojiRow>;
}
