import EmojiCollapsible from "@/components/emoji/EmojiCollapsible"

const generalEmojis = [
    {
        emoji: "❤️",
        emojiDesc: "Current hitpoints",
    },
    {
        emoji: "🖤",
        emojiDesc: "Lost hitpoints",
    },
    {
        emoji: "⭐",
        emojiDesc: "Level",
    },
    {
        emoji: "✨",
        emojiDesc: "Experience Points (XP)",
    },
    {
        emoji: "🗡️",
        emojiDesc: "One-Handed",
    },
    {
        emoji: "⚔️",
        emojiDesc: "Melee, Two-Handed, Fight",
    },
    {
        emoji: "🏹",
        emojiDesc: "Ranged",
    },
    {
        emoji: "🔀",
        emojiDesc: "Mixed (Melee + Ranged)",
    },
    {
        emoji: "👊",
        emojiDesc: "Unarmed Strike",
    },
    {
        emoji: "L",
        emojiDesc: "Light",
    },
    {
        emoji: "T",
        emojiDesc: "Thrown",
    },
    {
        emoji: "V",
        emojiDesc: "Versatile",
    },
    {
        emoji: "🛡️",
        emojiDesc: "Armor, Shield, Armor Class (AC)",
    },
    {
        emoji: "🪶",
        emojiDesc: "Flying Monster",
    },
    {
        emoji: "💧",
        emojiDesc: "Swimming Monster",
    },
    {
        emoji: "❕",
        emojiDesc: "Challenge Rating (CR)",
    },
    {
        emoji: "🪙",
        emojiDesc: "Gold (GP)",
    },
    {
        emoji: "🎲",
        emojiDesc: "Hit Dice",
    },
    {
        emoji: "🗺️",
        emojiDesc: "Campaign Number/ID",
    },
    {
        emoji: "🔄",
        emojiDesc: "Restarted/Resumed",
    },
];

const monsterEmojis = [
    {
        emoji: "🦑",
        emojiDesc: "Aberration",
    },
    {
        emoji: "🦁",
        emojiDesc: "Beast",
    },
    {
        emoji: "👼",
        emojiDesc: "Celestial",
    },
    {
        emoji: "🤖",
        emojiDesc: "Construct",
    },
    {
        emoji: "👹",
        emojiDesc: "Demon",
    },
    {
        emoji: "👿",
        emojiDesc: "Devil",
    },
    {
        emoji: "🐉",
        emojiDesc: "Dragon",
    },
    {
        emoji: "🔥",
        emojiDesc: "Elemental",
    },
    {
        emoji: "👺",
        emojiDesc: "Fiend",
    },
    {
        emoji: "🗿",
        emojiDesc: "Giant",
    },
    {
        emoji: "🧍‍♂️",
        emojiDesc: "Humanoid",
    },
    {
        emoji: "🧟",
        emojiDesc: "Monstrosity",
    },
    {
        emoji: "🦠",
        emojiDesc: "Ooze",
    },
    {
        emoji: "🌱",
        emojiDesc: "Plant",
    },
    {
        emoji: "💀",
        emojiDesc: "Undead",
    },
    {
        emoji: "🦂",
        emojiDesc: "Yugoloth",
    },
];

const classesEmojis = [
    {
        emoji: "⚒️",
        emojiDesc: "Barbarian",
    },
    {
        emoji: "📯",
        emojiDesc: "Bard",
    },
    {
        emoji: "☀️",
        emojiDesc: "Cleric",
    },
    {
        emoji: "🌿",
        emojiDesc: "Druid",
    },
    {
        emoji: "⚔️",
        emojiDesc: "Fighter",
    },
    {
        emoji: "☯",
        emojiDesc: "Monk",
    },
    {
        emoji: "🛡️",
        emojiDesc: "Paladin",
    },
    {
        emoji: "🏹",
        emojiDesc: "Ranger",
    },
    {
        emoji: "🗡️",
        emojiDesc: "Rogue",
    },
    {
        emoji: "💫",
        emojiDesc: "Sorcerer",
    },
    {
        emoji: "🧿",
        emojiDesc: "Warlock",
    },
    {
        emoji: "🔮",
        emojiDesc: "Wizard",
    },
];

export default function EmojiTable()
{
    return (
        <table>
            <thead>
                <tr>
                    <th>Emoji</th>
                    <th>Use</th>
                </tr>
            </thead>
            <EmojiCollapsible  emojis={generalEmojis} tableTitle="General"></EmojiCollapsible>
            <EmojiCollapsible emojis={monsterEmojis} tableTitle="Monster Types"></EmojiCollapsible>
            <EmojiCollapsible emojis={classesEmojis} tableTitle="Classes"></EmojiCollapsible>
        </table>
    );
}
