# Dungeons Website

Future repo for the Dungeons website, containing live stats of campaigns, past stats, rules & tips, etc.

## Features

- [ ] Emoji descriptions
- [ ] Description of differences between "standard" D&D
- [ ] Tips for play
- [ ] D&D rules
- [ ] Active bots and their follower counts
- [ ] Links to other resources, libraries, and information
---
- [ ] Current character/campaign information/stats
- [ ] Past character/campaign information/stats
---
- [ ] [D&D API](https://www.dnd5eapi.co/) browser
